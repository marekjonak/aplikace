SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `adress`;
CREATE TABLE `adress` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `street` char(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `descriptive_number` int(11) DEFAULT NULL,
  `orientation_number` int(11) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `adress` (`id`, `city`, `street`, `descriptive_number`, `orientation_number`, `zip`) VALUES
(1,	'0',	'dfgsdf',	1,	2,	12335),
(2,	'gfsh',	'fghfg',	2,	3,	12345),
(3,	'gfsh',	'fghfg',	2,	3,	12345),
(4,	'sfghsfgh',	'sgfhgfs',	3,	0,	0),
(5,	'sfghsfgh',	'sgfhgfs',	3,	0,	0),
(6,	'sfghsfgh',	'sgfhgfs',	3,	0,	0),
(7,	'dfgsdfg',	'pbvj',	3,	5,	55555),
(8,	'jhgm',	'ghfjkjh',	9,	5,	6654);

DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) COLLATE utf8_czech_ci NOT NULL,
  `ico` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `company_adress`;
CREATE TABLE `company_adress` (
  `company_id` int(11) NOT NULL,
  `adress_id` int(10) unsigned NOT NULL,
  KEY `company_id` (`company_id`),
  KEY `adress_id` (`adress_id`),
  CONSTRAINT `company_adress_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `company_adress_ibfk_2` FOREIGN KEY (`adress_id`) REFERENCES `adress` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
