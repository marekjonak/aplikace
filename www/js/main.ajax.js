$(function () {
    //init nette ajax
    $.nette.init();
});

/**
 * Conditional including of extensions
 *
 * @param string name
 * @param object events
 * @param object context
 */
function addConditionalExtension(name, events, context) {
    $.nette.ext(name, $.extend(events, {
        prepare: function (settings) {
            var ajaxOn = settings.nette.el.data('ajax-on');
            if (!settings.nette || !(ajaxOn instanceof Array) || $.inArray(name, ajaxOn) < 0) {
                if (!settings.off) {
                    settings.off = [];
                }
                //settings.off.push(name);
            }
        }
    }), context)
}

