// AJAX fade out
$(function() {
    $.nette.ext('animate', {
        complete: function() {
            $('.animate').animate({
                opacity: 1.0
            }, 3000).fadeOut(2000);
        }
    });
});

// standard fade out
$(document).ready(function() {
    $('.animate').animate({
        opacity: 1.0
    }, 3000).fadeOut(2000);
});