<?php
/**
 * Created by PhpStorm.
 * User: mjonak
 * Date: 10.8.15
 * Time: 10:46
 */

namespace App\Presenters;
use Nette;
use Nette\Application\UI\Form;
use Nette\Caching\Cache;

class CompanyPresenter extends BasePresenter
{
	/** @var \DibiConnection @inject */
	public $database;

	protected function createComponentShowCompany($name)
	{
		$grid = new \Grido\Grid($this,$name);
		$grid->setRememberState(false);
		$grid->translator->setLang('cs');

		$data =  $this->database->select('[co].[id] as companyId, [co].[name],[co].[ico],[a].[*]')->from('[company] as [co]')
				->leftJoin('[company_adress] as [ca]')
				->on('[co].[id] = [ca].[company_id]')
				->leftJoin('[adress] as [a]')
				->on('[ca].[adress_id] = [a].[id]');


		$grid->setModel($data);

		$grid->addColumnText('companyId', 'ID')
				->setSortable()
				->setFilterNumber();

		$grid->addColumnText('ico','ICO')
				->setSortable()
				->setFilterText()
				->setSuggestion();

		$grid->addColumnText('name','Name')
				->setSortable()
				->setFilterText()
				->setSuggestion();
		$grid->addColumnText('city','City')
				->setSortable();
		$grid->addColumnText('street','Street')
				->setSortable();
		$grid->addColumnText('orientation_number','Orientation number')
				->setSortable();
		$grid->addColumnText('descriptive_number','Descriptive Number')
				->setSortable();
		$grid->addColumnText('zip','Zip')
				->setSortable();

		$grid->addFilterCheck('check','tisknout');

		$grid->addActionHref('edit', 'Upravit')
				->setCustomHref((function ($row) {
					return $this->link('Company:edit', array("adress" => $row['id'],"companyId" => $row['companyId']));
				}));

	}

	public function createComponentInsertCompanyForm()
	{
		$form = new Form;
		$form->addText('name', 'Name:')
				->setRequired('Please enter company name.');
		$form->addText('ico', 'ICO:')
				->setRequired('Please enter ICO.');
		$form->addText('city','City');
		$form->addText('street','Street');
		$form->addText('descriptive_number','Descriptive number');
		$form->addText('orientation_number','Orientation number');
		$form->addText('zip','Zip');
		$form->addSubmit('send', 'Uložit');

		$form->onSuccess[] = array($this, 'insertCompanyFormSucceeded');
		return $form;
	}

	public function insertCompanyFormSucceeded(\Nette\Application\UI\Form $form)
	{
		$values = $form->getValues();
		$companyID = $this->getParameter('companyId');
		$adress = $this->getParameter('adress');
		if($companyID)
		{
			$this->database->query('UPDATE [adress] SET'
					. '[city] = %s,'
					. '[street] = %s,'
					. '[descriptive_number] = %i,'
					. '[orientation_number] = %i,'
					. '[zip] = %i',$values->city,$values->street,$values->descriptive_number,$values->orientation_number,$values->zip,
						'WHERE `id`=%i', $adress);

			$this->database->query('UPDATE [company] SET'
					. '[name] = %s,'
					. '[ICO] = %s'
					,$values->name,$values->ico,'WHERE `id`=%i', $companyID);
			$this->flashMessage("Společnost upravena", 'success');
			$this->redirect('default');
		}
		else
		{
		$this->database->query('INSERT INTO [adress] SET'
				. '[city] = %s,'
				. '[street] = %s,'
				. '[descriptive_number] = %i,'
				. '[orientation_number] = %i,'
				. '[zip] = %i',$values->city,$values->street,$values->descriptive_number,$values->orientation_number,$values->zip);

		$adressId = $this->database->getInsertId();

		$this->database->query('INSERT INTO [company] SET'
				. '[name] = %s,'
				. '[ICO] = %s'
				,$values->name,$values->ico);

		$companyId = $this->database->getInsertId();

		$this->database->query('INSERT INTO [company_adress] SET'
				. '[adress_id] = %i,'
				. '[company_id] = %i'

				,$adressId,$companyId);
		$this->flashMessage("Společnost přidána", 'success');
		$this->redirect('default');
		}

	}
	public function actionEdit()
	{
		$companyID = $this->getParameter('companyId');
		$post = $this->database->select('[*]')->from('[company] as [co]')
				->leftJoin('[company_adress] as [ca]')
				->on('[co].[id] = [ca].[company_id]')
				->leftJoin('[adress] as [a]')
				->on('[ca].[adress_id] = [a].[id]')
				->where('[co].[id] = %i',$companyID)
				->fetchAll();
		if (!$post) {
			$this->error('Příspěvek nebyl nalezen');
		}
		$this['insertCompanyForm']->setDefaults($post[0]);
	}

	public function actionShowAdress()
	{
		$post = $this->database->select('[*]')->from('[adress]');
		$this->template->adress = $post;
	}
} 
