<?php

namespace App\Presenters;


use App\Model\ArticleManager;

class ArticlePresenter extends BasePresenter
{
	/** @var ArticleManager @inject  */
	public $articleManager;


	protected function createComponentShowArticle($name)
	{
		$grid = new \Grido\Grid($this,$name);
		$grid->setRememberState(false);
		$grid->translator->setLang('cs');
		$data = $this->articleManager->getData();

		$grid->setModel($data);

		$grid->addColumnText('id', 'ID')
				->setSortable()
				->setFilterNumber();

		$grid->addColumnText('title','Titulek')
				->setSortable()
				->setFilterText()
				->setSuggestion();

		$grid->addColumnText('content','Obsah')
				->setSortable()
				->setFilterText()
				->setSuggestion();
		$grid->addColumnText('created_at','Vytvořeno')
				->setSortable();
	}
} 
