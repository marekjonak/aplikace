<?php

namespace App\Presenters;

use Nette;
use App\Model;


class HomepagePresenter extends BasePresenter
{

	/** @var \DibiConnection @inject */
	public $database;


	public function renderDefault()
	{
		$this->template->companies = $this->database->select('[co].[id] as companyId, [co].[name],[co].[ico],[a].[*]')->from('[company] as [co]')
				->leftJoin('[company_adress] as [ca]')
				->on('[co].[id] = [ca].[company_id]')
				->leftJoin('[adress] as [a]')
				->on('[ca].[adress_id] = [a].[id]');
	}

}
