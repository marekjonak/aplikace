<?php
namespace App\Presenters;



use Nette\Application\UI\Form;

class PostPresenter extends BasePresenter
{
	/** @var \DibiConnection @inject */
	public $database;

	public function renderShow($postId)
	{
		$conditions = [
				['[id] = %i', $postId],
		];
		$post = $this->database->select('[*]')->from('[posts]')->where($conditions)->fetch();
		if (!$post) {
			$this->error('Stránka nebyla nalezena');
		}

		$this->template->post = $post;
	}
	protected function createComponentCommentForm()
	{
		$form = new Form;

		$form->addText('name', 'Jméno:')
				->setRequired();

		$form->addText('email', 'Email:');

		$form->addTextArea('content', 'Komentář:')
				->setRequired();

		$form->addSubmit('send', 'Publikovat komentář');

		$form->onSuccess[] = array($this, 'commentFormSucceeded');

		return $form;
	}

	public function commentFormSucceeded(Form $form, $values)
	{
		$postId = $this->getParameter('postId');

		$this->database->query('INSERT INTO [comments] SET '
				. '[post_id] = %i,'
				. '[name] = %s,'
				. '[email] = %s,'
				. '[content] = %s',$postId,$values->name,$values->email,$values->content);

		$this->flashMessage('Děkuji za komentář', 'success');
		$this->redirect('this');
	}

} 
