<?php
namespace App\Model;


use Nette\Object;

class ArticleManager extends Object
{
	/** @var \DibiConnection*/
	public $database;

	public function __construct(\DibiConnection $connection)
	{
		$this->database = $connection;
	}

	public function getData()
	{
		return $this->database->select('[*]')->from('[posts]');
	}
} 
